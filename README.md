# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://lp2001@bitbucket.org/lp2001/stroboskop.git
mv stroboskop/ domacaNaloga1
cd domacaNaloga1

Naloga 6.2.3:

https://bitbucket.org/lp2001/stroboskop/commits/2c5ab1071d48b95f918110fd7e3ce85138c53454

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:

https://bitbucket.org/lp2001/stroboskop/commits/59f2e76a2ab1f3c436592f545aa7d0f45b89d839

Naloga 6.3.2:

https://bitbucket.org/lp2001/stroboskop/commits/ab624e63b260160cfa249013fa62b089f367c18c

Naloga 6.3.3:

https://bitbucket.org/lp2001/stroboskop/commits/965028b8a75d317afc93fe855621035956084298

Naloga 6.3.4:

https://bitbucket.org/lp2001/stroboskop/commits/f2901a3675b2cfacd6c0efa067d97252a96c4d5b

Naloga 6.3.5:

git commit -a -m "Združitev veje izgled"
git push origin izgled
git checkout master
git merge izgled
git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:

https://bitbucket.org/lp2001/stroboskop/commits/2fc9f042d9383cc03d05c2d9c2f4655d956134b5

Naloga 6.4.2:

https://bitbucket.org/lp2001/stroboskop/commits/18928c133cf846dd8472f47918b6c2b511a6060bw

Naloga 6.4.3:

https://bitbucket.org/lp2001/stroboskop/commits/4995dd336623105eee29b5e11c8e2839a187835b

Naloga 6.4.4:

https://bitbucket.org/lp2001/stroboskop/commits/471b436357deb0571f889f6238d50ef83c77a696